-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2014 at 12:06 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `skilltest`
--
CREATE DATABASE IF NOT EXISTS `skilltest` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `skilltest`;

-- --------------------------------------------------------

--
-- Table structure for table `ticker`
--

CREATE TABLE IF NOT EXISTS `ticker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `high` float NOT NULL,
  `low` float NOT NULL,
  `avg` float NOT NULL,
  `vol` float NOT NULL,
  `vol_cur` float NOT NULL,
  `last` float NOT NULL,
  `buy` float NOT NULL,
  `sell` float NOT NULL,
  `highlow_diff` float NOT NULL,
  `highlast_diff` float NOT NULL,
  `lowlast_diff` float NOT NULL,
  `updated` int(20) NOT NULL,
  `server_time` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ticker`
--

INSERT INTO `ticker` (`id`, `high`, `low`, `avg`, `vol`, `vol_cur`, `last`, `buy`, `sell`, `highlow_diff`, `highlast_diff`, `lowlast_diff`, `updated`, `server_time`) VALUES
(1, 585.85, 565.561, 575.706, 2205400, 3828.03, 571.172, 571.172, 568.579, 20.289, 14.678, -5.61103, 1403673395, 1403673396),
(2, 585.85, 565.561, 575.706, 2210170, 3836.44, 569.008, 571.676, 569.001, 20.289, 16.842, -3.44703, 1403674296, 1403674296),
(3, 585.85, 565.561, 575.706, 2217080, 3848.61, 569.2, 570.727, 569.2, 20.289, 16.65, -3.63903, 1403675195, 1403675197),
(4, 585.85, 565.561, 575.706, 2219780, 3853.54, 571.889, 571.889, 569.8, 20.289, 13.961, -6.32803, 1403676096, 1403676097),
(5, 585.85, 565.561, 575.706, 2303700, 4001.77, 569.08, 569.999, 568.86, 20.289, 16.77, -3.51903, 1403676995, 1403676997),
(6, 585.85, 565.561, 575.706, 2320110, 4030.66, 568.085, 568.085, 568, 20.289, 17.765, -2.52403, 1403677896, 1403677897),
(7, 585.85, 564.1, 574.975, 2402250, 4176.07, 567.998, 567.999, 567.165, 21.75, 17.852, -3.89802, 1403678795, 1403678797),
(8, 585.85, 562.941, 574.396, 2492050, 4336.25, 563.006, 564.097, 563.97, 22.909, 22.844, -0.06502, 1403679698, 1403679699),
(9, 585.85, 561.143, 573.497, 2548630, 4437.27, 562.594, 562.594, 562.002, 24.707, 23.256, -1.45099, 1403680595, 1403680596);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
