<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __constructor(){
		parent::__constructor();	
	}
	
	public function index()
	{
		$this->load->view('home-view');
	}
	
	public function get_search(){
		$this->load->model('home_model');
		$result = $this->home_model->get_data($_POST['daterange']);
		foreach($result as $key => $val){
			$high[] = $result[$key]['high'];
			$low[] = $result[$key]['low'];
			$sell[] = $result[$key]['sell'];
		}
		
		$data['lowest_sell'] = min($sell); 
		$data['highest_sell'] = max($sell); 
		$data['current_sell'] = $result[0]['sell']; 
		$data['lowest_low'] = min($low); 
		$data['current_low'] = $result[0]['low']; 
		$data['highest_high'] = max($high); 
		$data['current_high'] = $result[0]['high']; 
		
		$hh_Index = array_search(max($high), $high);
		$data['hh_date'] = date('jS M Y G:i',$result[$hh_Index]['server_time']) ;
		$hs_Index = array_search(max($sell), $sell);
		$data['hs_date'] = date('jS M Y G:i',$result[$hs_Index]['server_time']) ;
		$ls_Index = array_search(min($sell), $sell);
		$data['ls_date'] = date('jS M Y G:i',$result[$ls_Index]['server_time']) ;
		$ll_Index = array_search(min($low), $low);
		$data['ll_date'] = date('jS M Y G:i',$result[$ll_Index]['server_time']) ;
		$data['c_date'] = date('jS M Y G:i',$result[0]['server_time']) ;
		
		$data['result'] = $result;
		$this->load->view('records-view',$data);
	}
	
	public function get_contents(){
		$this->load->model('home_model');
		$result = $this->home_model->get_contents();
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */