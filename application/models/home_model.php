<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
	
	function __constructor(){
		parent::__constructor();	
	}
	
	public function get_contents()
	{
		$data = file_get_contents("https://btc-e.com/api/2/btc_usd/ticker");
		$data = json_decode($data);
		$data = $data->ticker;
		$newdata = array(
			'high' => $data->high,
			'low' => $data->low,
			'avg' => $data->avg,
			'vol' => $data->vol,
			'vol_cur' => $data->vol_cur,
			'last' => $data->last,
			'buy' => $data->buy,
			'sell' => $data->sell,
			'highlow_diff' => ($data->high - $data->low),
			'highlast_diff' => ($data->high - $data->last),
			'lowlast_diff' => ($data->low - $data->last),
			'updated	' => $data->updated,
			'server_time' => $data->server_time
		);
		$this->save_data($newdata);
	}
	
	public function save_data($data){
		$this->db->insert('ticker',$data);	
	}
	
	public function get_data($daterange = false){
		$result = array();
		if(!$daterange){
			$this->db->order_by('server_time','desc');	
		}else{
			$date = explode(' to ',$daterange);
			//$this->db->where('server_time >=',strtotime($date[0]));
			//$this->db->where('server_time <=',strtotime($date[1]));
			$this->db->where('server_time BETWEEN "'. strtotime($date[0]) . '" and "'. strtotime($date[1]) .'"');
			$this->db->order_by('server_time','desc');
		}	
		$query = $this->db->get('ticker');
		if($query->num_rows()>0){
			$result = $query->result_array();	
		}
		return $result;
	}
}

/* End of file welcome.php */
/* Location: ./application/models/home_model.php */