                            <div class="widget" >            
                                <div class="widget-header">
                                  <div class="title">
                                    <span class="fs1" aria-hidden="true" data-icon="&#xe098;"></span> Records
                                  </div>
                                </div>
                                <div class="widget-body" >
                              		<?php if(count($result)>0):?>
                                    <ul class="stats-overview">
                                        <li>
                                          <span class="name">
                                            <strong>Lowest Low</strong>
                                            <br /><small><?php echo $ll_date;?></small>
                                          </span>
                                          <span class="value text-error">
                                            <?php echo $lowest_low;?>
                                          </span>
                                        </li>
                                        <li>
                                          <span class="name">
                                            <strong>Lowest Market Price</strong>
                                            <br /><small><?php echo $ls_date;?></small>
                                          </span>
                                          <span class="value text-success">
                                            <?php echo $lowest_sell;?>
                                          </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name">
                                            <strong>Highest Market Price</strong>
                                            <br /><small><?php echo $hs_date;?></small>
                                          </span>
                                          <span class="value text-info">
                                            <?php echo $highest_sell;?>
                                          </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name">
                                            <strong>Highest High</strong>
                                            <br /><small><?php echo $hh_date;?></small>
                                          </span>
                                          <span class="value text-warning">
                                            <?php echo $highest_high;?>
                                          </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name">
                                            <strong>Current High</strong>
                                            <br /><small><?php echo $c_date;?></small>
                                          </span>
                                          <span class="value text-error">
                                            <?php echo $current_high;?>
                                          </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name">
                                            <strong>Current Market Price</strong>
                                            <br /><small><?php echo $c_date;?></small>
                                          </span>
                                          <span class="value text-success">
                                            <?php echo $current_sell;?>
                                          </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name">
                                            <strong>Current Low</strong>
                                            <br /><small><?php echo $c_date;?></small>
                                          </span>
                                          <span class="value text-info">
                                            <?php echo $current_low;?>
                                          </span>
                                        </li>
                                    </ul>
                    
                                    <div id="dt_example" class="example_alt_pagination">
                                        <table class="table table-condensed table-striped table-hover table-bordered" id="data-table2">    
                                            <thead>
                                                <tr>
                                                    <th >High</th>
                                                    <th >Low</th>
                                                    <th >Avg</th>
                                                    <th >Vol</th>
                                                    <th >Vol_cur</th>
                                                    <th >Last</th>
                                                    <th >Buy</th>
                                                    <th >Sell</th>
                                                    <th >High - Low</th>
                                                    <th >High - Last</th>
                                                    <th >Low - Last</th>
                                                    <th >Date</th>
                                                </tr>
                                            </thead>
                                            <tbody id="">
                                                
                                            <?php foreach($result as $key => $value):?>    
                                                <tr>
                                                    <td><?php  echo $value['high'];?></td>
                                                    <td><?php  echo $value['low'];?></td>
                                                    <td><?php  echo $value['avg'];?></td>
                                                    <td><?php  echo $value['vol'];?></td>
                                                    <td><?php  echo $value['vol_cur'];?></td>
                                                    <td><?php  echo $value['last'];?></td>
                                                    <td><?php  echo $value['buy'];?></td>
                                                    <td><?php  echo $value['sell'];?> </td>
                                                    <td><?php  echo $value['highlow_diff'];?> </td>
                                                    <td><?php  echo $value['highlast_diff'];?> </td>
                                                    <td><?php  echo $value['lowlast_diff'];?> </td>
                                                    <td><?php  echo date('jS M Y G:i',$value['server_time']);?> </td>
                                                </tr>
                                            <?php endforeach;?>    
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php else:?>
                                    <div class="alert alert-block alert-error" align="center">No records found</div>
									<?php endif;?>
                                </div>            
                             </div>  