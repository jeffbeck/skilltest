<!DOCTYPE html>
  <!--[if lt IE 7]>
    <html class="lt-ie9 lt-ie8 lt-ie7" lang="en">
  <![endif]-->

  <!--[if IE 7]>
    <html class="lt-ie9 lt-ie8" lang="en">
  <![endif]-->

  <!--[if IE 8]>
    <html class="lt-ie9" lang="en">
  <![endif]-->

  <!--[if gt IE 8]>
    <!-->
    <html lang="en">
    <base href="<?php echo base_url();?>" />
    <!--
  <![endif]--><head>
    <meta charset="utf-8">
    <title>Kodenta Skill Test</title>
    <meta name="author" content="Jeff Beck Acain">
    <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
    <meta name="description" content="Redirect Monitoring">
    <script src="js/html5-trunk.js"></script>
    <link href="icomoon/style.css" rel="stylesheet">
    <!--[if lte IE 7]>
    <script src="css/icomoon-font/lte-ie7.js"></script>
    <![endif]-->
    <!-- bootstrap css -->
    <link href="css/bootstrap.css" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluid">
        <div class="dashboard-wrapper">
            <!--START #main_content-->
            <div id="main_content" >
                <div class="main-container">
                    <div class="page-header">
                        <div class="pull-left">
                          <h2>Kodenta Skill Test</h2>
                        </div>
                        <div class="pull-left">
                          <span id="preloader"></span>
                        </div>
                        <div class="pull-right">
                          <ul class="stats">
                            <li class="color-second hidden-phone">
                              <span class="fs1" aria-hidden="true" data-icon=""></span>
                              <div class="details" id="date-time"><span class="big"><?php echo date("M d, Y");?></span></div>
                            </li>
                          </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="control-group pull-left">
                          <div class="controls">
                            <div class="input-append">
                              <input type="text" name="txt-date" id="txt-date" class="input-large" placeholder="Select Date Range"/>
                              <span class="add-on" ><i class="icon-calendar" id="date-picker-icon"></i></span>
                            </div>
                          </div>
                        </div>
                        <div class="btn btn-info pull-left" id="search-btn" style="margin-left:5px;">Search</div>
                      </div>    
                                  
                      <div class="row-fluid">
                        <div class="span12 " id="records">
                        
                        </div>
                    </div>
                 </div>
            	<!--END #main_container-->   
            </div>
            <!--END #main_content-->
        </div><!-- dashboard-wrapper -->
    </div><!-- container-fluid -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/date.js"></script>
    <script type="text/javascript" src="js/daterangepicker.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript">
	var updateInterval = setInterval(function() {
		$('#preloader').html('<img src="<?php echo base_url();?>img/loading-orange.gif" /><small>retrieving data from server.</small>');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url();?>home/get_contents",
			success: function(data){$('#preloader').html('');}	
		});
	},900000);
	
	$(document).ready(function(){
		getdata();
		$('#txt-date').daterangepicker({
		  opens: 'right',
		  format: 'yyyy-MM-dd',
		  separator: ' to '
		});
		
		$('#date-picker-icon').click(function(){
			$('#txt-date').click();	
		});	
		
		$('#search-btn').click(function(){
			if($('#txt-date').val() != ''){
				getdata();
			}
		});
	});
	
	function getdata(){
		$('#records').html('<img src="<?php echo base_url();?>img/loading.gif" style="margin:50%;" />');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>home/get_search",
			data: {daterange:$('#txt-date').val()},
			success: function(data){$('#records').html(data);}	
		});
	}
	
    </script>
  </body>
</html>