- I used CodeIgniter framework on the test, you should be able to download the complete folders and files from bitbucket.

- these are the complete folders/files
1. application (folder)
2. css (folder)
3. icomoon (folder)
4. img (folder)
5. js (folder)
6. system (folder)
7. .htaccess (file)
8. index.php (file)
9. skilltest.sql

- The folder "skilltest" should be on your local webserver e.g c:/xampp/htdocs/
- Import "skilltest.sql" on your localhost database with username: root and and no password

If everything is complete, the test should be running.


